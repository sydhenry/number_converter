# Number Converter

This package is a class that allows for numbers to be represented in diffrent
bases that are common. Specfically, it allows any Hex, Decimal. or Binary
number to be converted to the others. While there are the hex() and bin()
built-in functions, this package allows for the number to be represented in any
of the forms which are generated as needed. The converting is done with methods
that are common for converting numbers by hand. While using te built-ins would
be faster, this helps renforce the process of converting bases by hand

# Examples

    :::python
    >>> from number import Number
    >>> foo = Number(123)
    >>> foo.hex_num
    '0x7B'
    >>> foo.bin_num
    '1111011_2'
    foo.hex = "FA31"
    foo.bin