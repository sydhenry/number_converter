from number import Number
import unittest




class NumberIs0Test(unittest.TestCase):

    num = Number(0)
    def testdec(self):
        self.assertTrue(self.num.dec_num=='0')
    def testhex(self):
        self.assertTrue(self.num.hex_num=='0x0')
    def testbin(self):
        self.assertTrue(self.num.bin_num=='0_2')

class NumberIs1Test(unittest.TestCase):

    num = Number(1)
    def testdec(self):
        self.assertTrue(self.num.dec_num=='1')
    def testhex(self):
        self.assertTrue(self.num.hex_num=='0x1')
    def testbin(self):
        self.assertTrue(self.num.bin_num=='1_2')

class NumberIs15Test(unittest.TestCase):

    num = Number('15')
    def testdec(self):
        self.assertTrue(self.num.dec_num=='15')
    def testhex(self):
        self.assertTrue(self.num.hex_num=='0xF')
    def testbin(self):
        self.assertTrue(self.num.bin_num=='1111_2')

class NumberIs17Test(unittest.TestCase):

    num = Number('17')
    def testdec(self):
        self.assertTrue(self.num.dec_num=='17')
    def testhex(self):
        self.assertTrue(self.num.hex_num=='0x11')
    def testbin(self):
        self.assertTrue(self.num.bin_num=='10001_2')

def main():
    unittest.main()

if __name__ == '__main__':
    main()
