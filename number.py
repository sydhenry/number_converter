﻿import re

HEX = (
    ('A', 10),
    ('B', 11),
    ('C', 12),
    ('D', 13),
    ('E', 14),
    ('F', 15),
)
BIN = (
    ('0', '0000'),
    ('1', '0001'),
    ('2', '0010'),
    ('3', '0011'),
    ('4', '0100'),
    ('5', '0101'),
    ('6', '0110'),
    ('7', '0111'),
    ('8', '1000'),
    ('9', '1001'),
    ('A', '1010'),
    ('B', '1011'),
    ('C', '1100'),
    ('D', '1101'),
    ('E', '1110'),
    ('F', '1111'),
)


class Number(object):
    """Class that takes a number in binary, decimal or hexadecimal and
    converts it to the other two. At initialization, only the inputed and
    the number in hexadecimal form are created. Decimal and Binary are
    generated on a as-needed basis.

    Args:
      string (str, int): If the number is in decimal, it can either be
        an int or it can be a srt. For hex, the number must have `0x`
        at the start, it is not case sensitive. For a binary number
        there must be a `_2` appended so that it will be seen as a binary
        number and not a decimal.

    Attributes:
      hex_num (str): The number in hexadecimal notation.
      dec_num (str): The number in decimal notation.
      bin_num (str): The number in binary notation.

    Notes:
      Any of the models attributes can be changed to a new number. This will
      reinitialize the object and all of the values will be overwritten.
    """

    __hex_list = __bin_list = []
    _dec_num = _hex_num = _bin_num = None

    def __init__(self, string):
        if not isinstance(string, str):
            string = str(string)
        # Hexadecimal numbers
        if re.compile('^0[xX][\dA-Fa-f]+$').match(string):
            self._hex_num = string.upper()[2:]
        # Binary numbers
        elif re.compile('^[01]+_2$').match(string):
            self._bin_num = string[:-2]
            self._convert_bin_hex()
        # Decimal numbers
        elif re.compile('^\d+$').match(string):
            self._dec_num = int(string)
            self._convert_dec_hex()
        else:
            assert "Could not parse the given input"

    @property
    def _hex_list(self):
        """Helper method that allows for a hex number to be broken up into a
        list so that we can itterate though later to convert.
        """
        if not self.__hex_list:
            self.__hex_list = []
            self.__hex_list[:0] = self._hex_num
            self.__hex_list
        return self.__hex_list

    @property
    def _bin_list(self):
        """Helper method that allows for a binary number to be broken up into a
        list so that we can iterate though later to convert it to hex This will
        add the needed padding so that each element is a quartet.
        """
        if not self.__bin_list:
            _padded_bin_num = self._bin_num
            while len(_padded_bin_num) % 4:
                _padded_bin_num = '0' + _padded_bin_num
            for i in range(0, len(_padded_bin_num), 4):
                self.__bin_list.append(_padded_bin_num[i:i+4])
        return self.__bin_list

    @property
    def dec_num(self):
        # We have to handle the special case of 0
        if self._dec_num == 0:
            self._hex_num = '0'
        elif self._dec_num is None:
            self._convert_hex_dec()
        return str(self._dec_num)

    @dec_num.setter
    def dec_num(self, value):
        self.__init__(value)
        if self._bin_num:
            self._convert_hex_bin()

    @property
    def hex_num(self):
        return '0x' + self._hex_num

    @hex_num.setter
    def hex_num(self, value):
        self.__init__(value)
        if self._dec_num:
            self._convert_hex_dec()
        if self._bin_num:
            self._convert_hex_bin()

    @property
    def bin_num(self):
        if not self._bin_num:
            self._convert_hex_bin()
        return self._bin_num + '_2'

    @bin_num.setter
    def bin_num(self, value):
        self.__init__(value)
        if self._dec_num:
            self._convert_hex_dec()

    def _convert_hex_dec(self):
        self._dec_num = 0
        for pos, num in enumerate(reversed(self._hex_list)):
            found_dec_num = [dec for hex, dec in HEX if num == hex]
            if len(found_dec_num):
                found_dec_num = int(found_dec_num[0])
            else:
                found_dec_num = int(num)
            self._dec_num += found_dec_num * (16 ** pos)

    def _convert_hex_bin(self):
        self._bin_num = ''
        for cur in self._hex_list:
            found_bin_num = [bin for hex, bin in BIN if cur == hex][0]
            self._bin_num += found_bin_num
        self._bin_num = str(int(self._bin_num))

    def _convert_bin_hex(self):
        self._hex_num = ''
        for cur in self._bin_list:
            found_hex_num = [hex for hex, bin in BIN if cur == bin][0]
            self._hex_num += found_hex_num

    def _convert_dec_hex(self):
        self._hex_num = ''
        working_dec = int(self.dec_num)
        while working_dec:
            mod = working_dec % 16
            if mod > 9:
                found_hex_num = [hex for hex, dec in HEX if mod == dec][0]
            else:
                found_hex_num = str(mod)
            self._hex_num = found_hex_num + self._hex_num
            working_dec = working_dec//16

    def __repr__(self):
        return self.dec_num

    def __dict__(self):
        return {'dec_num': self.dec_num,
                'hex_num': self.hex_num,
                'bin_num': self.bin_num}
